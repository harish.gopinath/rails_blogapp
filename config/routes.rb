Rails.application.routes.draw do
  get 'books/index'
  root "articles#index"

  resources :articles do
    resources :comments
  end

  resources :books

  resources :products do
    get 'preview', on: :member
  end
  resources :people

  resources :people, only: [:index, :show, :new, :create]
end
