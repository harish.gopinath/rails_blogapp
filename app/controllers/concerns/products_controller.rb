# app/controllers/products_controller.rb
class ProductsController < ApplicationController
  def show
    @product = Product.find(params[:id])
  end

  def preview
    @product = Product.find(params[:id])
    render template: "products/show"
  end
end
