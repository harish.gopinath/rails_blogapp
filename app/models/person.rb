class Person < ApplicationRecord
  validates :name, presence: true
  validates :terms_of_service, acceptance: true
  validates :terms_of_service, acceptance: { message: 'must be abided' }
  validates :terms_of_service, acceptance: { accept: 'yes' }
  validates :eula, acceptance: { accept: ['TRUE', 'accepted'] }
  validates :email, confirmation: true
  validates :email, confirmation: { case_sensitive: false }
  validates :end_date, comparison: { greater_than: :start_date }
  validates :legacy_code, format: { with: /\A[a-zA-Z]+\z/,
    message: "only allows letters" }

    validates_each :name, :surname do |record, attr, value|
      record.errors.add(attr, 'must start with upper case') if /\A[[:lower:]]/.match?(value)
    end

    validates :name, presence: true, length: { minimum: 3 }

end
